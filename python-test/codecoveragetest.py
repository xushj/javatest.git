import coverage
import random


'''
 测试条件判断 https://coverage.readthedocs.io/en/latest/
 pip3 install coverage
 # python ./codecoveragetest.py
 coverage run ./codecoveragetest.py
 coverage report
 coverage html 
'''


def test_if(num):
    if num < 10:
        print("num < 10")
    elif num < 30:
        print("num < 30")
    elif num < 60:
        print("num < 60")
    elif num < 100:
        print("num < 100")
    else:
        print(num)


cov = coverage.Coverage()
cov.start()

# .. call your code ..
i = 5
while i > 0:
    num = random.randint(1, 110)
    test_if(num)
    i = i-1
cov.stop()
cov.save()

cov.html_report()
