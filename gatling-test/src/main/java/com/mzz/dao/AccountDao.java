package com.mzz.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mzz.entity.Account;

/**
 * JPA DAO 层接口
 *
 * @author xushijian
 *
 */
public interface AccountDao extends JpaRepository<Account, Integer> {

}
