package com;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * 异常：
 *
 * Error creating bean with name 'entityManagerFactory' defined in class path
 * resource [org/springframewor
 *
 * 	造成原因： 注解引入错误 ！
 *
 *
 * JPA 测试 Hello world!
 *
 * 访问：http://localhost:8080/account/list
 *
 *
 */
@SpringBootApplication
public class SpringBootJpaApplication {

	public static void main(
			String[] args) {

		SpringApplication.run(SpringBootJpaApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(
			ApplicationContext ctx) {

		return args -> {
			System.out.println("start output BeanName :");

			String[] beanNames = ctx.getBeanDefinitionNames();

			Arrays.sort(beanNames);
			for (String bname : beanNames) {
				System.out.println("beanName is:" + bname);
			}
		};
	}

}
