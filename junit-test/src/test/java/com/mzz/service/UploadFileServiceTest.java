package com.mzz.service;

import com.mzz.JunitApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 上传文件服务测试
 *
 * @author xushijian
 * @date 2021/06/17 9:11
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JunitApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UploadFileServiceTest {

    /**
     * @LocalServerPort 提供了 @Value("${local.server.port}") 的代替
     */
    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setUp() throws Exception {
        String url = String.format("http://localhost:%d/", port);
        System.out.println(String.format("port is : [%d]", port));
        this.base = new URL(url);
    }


    /**
     * 模拟本地传文件信息
     */
    @Test
    public void uploadFileTest() {

        Map<String, Object> params = new HashMap<>();
        //  上传文件
        FileSystemResource resource = new FileSystemResource(new File("C:\\Users\\xusj\\Git\\mzz\\Python\\read_file\\output\\指标数据.xlsx"));
        params.put("file", resource);

        HttpHeaders httpHeaders = new HttpHeaders();
        params = params == null ? new LinkedHashMap<>() : params;

        MultiValueMap<String, Object> stringObjectLinkedMultiValueMap = new LinkedMultiValueMap<>();
        params.forEach((o1, o2) -> stringObjectLinkedMultiValueMap.add(o1, o2));

        String s = restTemplate.postForObject(this.base.toString() + "/file/upload", new HttpEntity(stringObjectLinkedMultiValueMap, httpHeaders), String.class);

        System.out.println("上传结果为：" + s);
    }

    @Test
    public void getUserTest() throws Exception {

        //序列化对象失败
        //        ResponseEntity<List> response = this.restTemplate.getForEntity(
        //                this.base.toString() + "/assets/newReleaseList", List.class, "");
        //设置Http的Header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        //设置访问参数
        HashMap<String, Object> params = new HashMap<>();

        //设置访问的Entity
        HttpEntity entity = new HttpEntity<>(params, headers);
        ResponseEntity<String> result = restTemplate.exchange(this.base.toString() + "/user/1", HttpMethod.GET, entity, String.class);
        //JSONObject data = JSONObject.parseObject(result.getBody()).getJSONObject("data");

        System.out.println(String.format("测试结果为：%s", result));
    }

}