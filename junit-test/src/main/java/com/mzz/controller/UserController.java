package com.mzz.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户控制层
 *
 * @author xushijian
 * @date 2021/06/17 17:01
 */
@RestController
@RequestMapping("user")
public class UserController {

    @GetMapping("/{id}")
    public String getUserName(@PathVariable("id") Long id) {
        return "mzz" + id;
    }
}