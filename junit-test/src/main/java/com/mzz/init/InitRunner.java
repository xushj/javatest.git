package com.mzz.init;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 初始化
 *
 * @author xushijian
 * @date 2021/06/17 17:12
 */
@Component
@Order(1)
@Slf4j
public class InitRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {

        log.info("开始初始化数据 ...");
        TimeUnit.SECONDS.sleep(1);
        log.info("结束初始化数据 ...");
    }
}